package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();

    @NotNull
    AdminUserEndpoint getAdminUserEndpoint();

    @NotNull
    SessionDTO getSession();

    void setSession(@Nullable SessionDTO session);

}
