package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.api.IService;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IUserService extends IService<User> {

    boolean checkRoles(@Nullable String userId, @Nullable Role... roles);

    void add(@NotNull User user);

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    List<User> findAll();

    @NotNull
    User findById(@NotNull String id);

    @Nullable
    User findByEmail(@Nullable String email);

    @Nullable
    User findByLogin(@Nullable String login);

    @NotNull
    User lockByLogin(@Nullable String login);

    void removeById(@NotNull String id);

    void removeByLogin(@Nullable String login);

    @NotNull
    User setPassword(@Nullable String userId, @Nullable String password);

    @NotNull
    User unlockByLogin(@Nullable String login);

    @NotNull
    User update(
            @Nullable String userId,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

}
